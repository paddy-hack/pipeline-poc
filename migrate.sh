#!/bin/sh -eu
# SPDX-License-Identifier: CC-BY-SA-4.0
# SPDX-FileCopyrightText: © 2022 Olaf Meeuwissen

echo "Goodbye Debian World!"
echo "... migrating, hold on to your seat ..."
echo "Hello Devuan World!"
